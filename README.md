# The Giffer

An iOS client to Giphy ( https://developers.giphy.com/)

## Getting Started

### Prerequisites

CocoaPods

### Building

Install the pods:

```
pod install
```

Open the workspace file and build the only target.

## Built With

* [CHTCollectionViewWaterfallLayout](https://github.com/chiahsien/CHTCollectionViewWaterfallLayout) - More compact UICollectionView layout
* [GiphyCoreSDK](https://github.com/Giphy/giphy-ios-sdk-core) - Giphy's iOS SDK
* [YYImage](https://github.com/ibireme/YYImage) - Used to render the gifs
* [Material Design Icons](https://material.io/tools/icons) - Source of the error icons

## Author

* **Philip White**

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details

