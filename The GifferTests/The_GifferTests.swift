//
//  The_GifferTests.swift
//  The GifferTests
//
//  Created by Philip White on 3/28/19.
//  Copyright © 2019 Philip White. All rights reserved.
//

import XCTest
import GiphyCoreSDK
import YYImage
@testable import The_Giffer

class GifItemTestDelegate : NSObject, GifItemThumbnailDelegate {
    let expectation : XCTestExpectation
    
    init(_ expectation: XCTestExpectation) {
        self.expectation = expectation
        super.init()
    }
    
    func gifItem(_ gifItem: GifItem, loadedThumbnail thumbnail: YYImage?, error: Error?) {
        print("Thumbnail size = \(String(describing: thumbnail?.size))")
        XCTAssert(error == nil && gifItem.thumbnailImage != nil, "Failed to download error")
        expectation.fulfill()
    }
    
    func gifItemWillRestartThumbnailDownload(_ gifItem: GifItem) {
    }
}

class GifListTestDelegate : NSObject, GifListDelegate {
    let expectation : XCTestExpectation
    let thumbnailExpectation : XCTestExpectation
    var gifItemDelegate : GifItemTestDelegate?
    
    init(_ expectation: XCTestExpectation, thumbnailExpectation: XCTestExpectation) {
        self.expectation = expectation
        self.thumbnailExpectation = thumbnailExpectation
        super.init()
    }
    
    func gifList(_ gifList: GifList, didFinishLoading error: Error?) {
        if let error = error {
            XCTFail(error.localizedDescription)
        } else {
            print("Found \(gifList.count) duck gifs")
            XCTAssert(gifList.count > 0, "No duck gifs found")
            
            if gifList.count > 0 {
                gifItemDelegate = GifItemTestDelegate(thumbnailExpectation)
                let gifItem = gifList.item(at: 0)
                gifItem.thumbnailDelegate = gifItemDelegate
                gifItem.loadThumbnail()
            }
        }
        expectation.fulfill()
    }
}

class The_GifferTests: XCTestCase {

    override func setUp() {
        GiphyCore.configure(apiKey: "ZsUpUm2L6cVbvei347EQNp7HrROjbOdc")
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testSearch() {
        let fetchListExpectation = XCTestExpectation(description: "Search results for \"duck\"")
        let fetchThumbnailExpectation = XCTestExpectation(description: "Thumbnail for first \"duck\" gif")
        
        let gifList = GifList(searchString: "duck")
        
        let gifListDelegate = GifListTestDelegate(fetchListExpectation, thumbnailExpectation: fetchThumbnailExpectation)
        
        gifList.delegate = gifListDelegate
        gifList.start()
        wait(for: [fetchListExpectation, fetchThumbnailExpectation], timeout: 20.0)
    }


}
