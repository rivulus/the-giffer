//
//  GifCollectionViewLoadingCell.swift
//  The Giffer
//
//  Created by Philip White on 3/30/19.
//  Copyright © 2019 Philip White. All rights reserved.
//

import UIKit

class GifCollectionViewLoadingCell: UICollectionViewCell {
    var spinnerView : UIActivityIndicatorView!
    var errorView : UIImageView!
    
    override func prepareForReuse() {
        spinnerView?.removeFromSuperview()
        spinnerView = nil
        errorView?.removeFromSuperview()
        errorView = nil
    }
    
    func configureAsSpinner() {
        if spinnerView == nil {
            spinnerView = UIActivityIndicatorView(style: .whiteLarge)
            contentView.addSubview(spinnerView)
            spinnerView.translatesAutoresizingMaskIntoConstraints = false
            spinnerView.centerXAnchor.constraint(equalTo: contentView.centerXAnchor).isActive = true
            spinnerView.centerYAnchor.constraint(equalTo: contentView.centerYAnchor).isActive = true
            spinnerView.color = UIColor.black
        }
        spinnerView.startAnimating()
    }
    
    func configureAsError() {
        if errorView == nil {
            spinnerView?.removeFromSuperview()
            errorView = UIImageView(image: UIImage(named:"ErrorLarge"))
            contentView.addSubview(errorView)
            errorView.translatesAutoresizingMaskIntoConstraints = false
            errorView.centerXAnchor.constraint(equalTo: contentView.centerXAnchor).isActive = true
            errorView.centerYAnchor.constraint(equalTo: contentView.centerYAnchor).isActive = true
        }
    }
}
