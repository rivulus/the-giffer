//
//  GifCollectionViewCell.swift
//  The Giffer
//
//  Created by Philip White on 3/29/19.
//  Copyright © 2019 Philip White. All rights reserved.
//

import UIKit
import YYImage

class GifCollectionViewCell: UICollectionViewCell, GifItemThumbnailDelegate {
    
    var imageView : UIImageView?
    var spinnerView : UIActivityIndicatorView?
    var thumbnail : UIImage?
    var error : Error?
    var gifItem : GifItem? {
        didSet {
            gifItem?.thumbnailDelegate = self
            gifItem?.loadThumbnail()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        thumbnail = nil
        error = nil
        gifItem?.thumbnailDelegate = nil
        gifItem = nil
        
        spinnerView?.removeFromSuperview()
        imageView?.removeFromSuperview()
        spinnerView = nil
        imageView = nil
    }
    
    func configureViews() {
        if let thumbnail = thumbnail {
            // If there is an image view, we've already configured this.
            if imageView == nil {
                spinnerView?.removeFromSuperview()
                imageView = YYAnimatedImageView(image: thumbnail)
                contentView.addSubview(imageView!)
                imageView!.translatesAutoresizingMaskIntoConstraints = false
                imageView!.centerXAnchor.constraint(equalTo: contentView.centerXAnchor).isActive = true
                imageView!.centerYAnchor.constraint(equalTo: contentView.centerYAnchor).isActive = true
            }
        } else {
            assert(imageView == nil)
            
            // If there is a spinner view, we've already configured this.
            if spinnerView == nil {
                spinnerView = UIActivityIndicatorView()
                contentView.addSubview(spinnerView!)
                spinnerView!.translatesAutoresizingMaskIntoConstraints = false
                spinnerView!.centerXAnchor.constraint(equalTo: contentView.centerXAnchor).isActive = true
                spinnerView!.centerYAnchor.constraint(equalTo: contentView.centerYAnchor).isActive = true
                spinnerView!.color = UIColor.black
                spinnerView!.startAnimating()
            }
        }
    }
    
    // MARK: GifItemDelegate
    
    func gifItem(_ gifItem: GifItem, loadedThumbnail thumbnail: YYImage?, error: Error?) {
        if let error = error {
            self.thumbnail = UIImage(named: "ErrorSmall")
            self.error = error
        } else {
            self.thumbnail = thumbnail
        }
        
        self.configureViews()
    }
    
    
    func gifItemWillRestartThumbnailDownload(_ gifItem: GifItem) {
        error = nil
        thumbnail = nil
        spinnerView?.removeFromSuperview()
        imageView?.removeFromSuperview()
        spinnerView = nil
        imageView = nil
        configureViews()
    }
}
