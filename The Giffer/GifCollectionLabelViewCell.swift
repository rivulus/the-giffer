//
//  GifCollectionLabelViewCell.swift
//  The Giffer
//
//  Created by Philip White on 4/1/19.
//  Copyright © 2019 Philip White. All rights reserved.
//

import UIKit

class GifCollectionLabelViewCell: UICollectionViewCell {
    @IBOutlet weak var label : UILabel!
    
    func configureAsNoSearch() {
        label.text = NSLocalizedString("ReadyToSearch", comment: "")
    }
    
    func configureAsNoResults() {
        label.text = NSLocalizedString("NoResults", comment: "")
    }
}
