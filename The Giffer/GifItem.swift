//
//  GifItem.swift
//  The Giffer
//
//  Created by Philip White on 3/28/19.
//  Copyright © 2019 Philip White. All rights reserved.
//

import UIKit
import GiphyCoreSDK
import YYImage

protocol GifItemThumbnailDelegate: class {
    func gifItem(_ gifItem: GifItem, loadedThumbnail thumbnail : YYImage?, error : Error?)
    func gifItemWillRestartThumbnailDownload(_ gifItem: GifItem)
}

protocol GifItemFullsizeDelegate: class {
    func gifItem(_ gifItem: GifItem, loadedFullsize image : YYImage?, error : Error?)
}

class GifItem: NSObject {
    
    enum FetchError: Error {
        case emptyURL
    }
    
    enum FetchType {
        case thumbnail
        case fullsize
    }
    
    weak var thumbnailDelegate : GifItemThumbnailDelegate?
    weak var fullsizeDelegate : GifItemFullsizeDelegate?
    
    let media : GPHMedia
    
    var title : String? {
        return media.title
    }
    
    var thumbnailDownloadTask : URLSessionDataTask?
    var fullsizeDownloadTask : URLSessionDataTask?
    
    var thumbnailMetadata : GPHImage?
    var fullsizeMetadata : GPHImage?
    
    var thumbnailImage : YYImage?
    var fullsizeImage : YYImage?
    
    var thumbnailError : Error?
    var fullsizeError : Error?
    
    init(_ media: GPHMedia) {
        self.media = media
        
        if let images = media.images {
            thumbnailMetadata = images.fixedWidthDownsampled
            fullsizeMetadata = images.original
        }
        
        super.init()
    }
    
    func loadThumbnail() {
        loadImage(.thumbnail)
    }
    
    func loadFullsizeImage() {
        loadImage(.fullsize)
    }
    
    func loadImage(_ fetchType: FetchType) {
        
        let metadata = (fetchType == .thumbnail) ? thumbnailMetadata : fullsizeMetadata
        
        if handleExistingDownloadTask(fetchType) {
            return
        }
        
        guard let urlString = metadata?.gifUrl, let url = URL(string: urlString) else {
            finish(fetchType, error: FetchError.emptyURL)
            return
        }
        
        print("loading")
        
        var urlRequest = URLRequest(url: url)
        urlRequest.addValue("image/*", forHTTPHeaderField: "accept")
        urlRequest.timeoutInterval = 300.0
    
        let downloadTask = URLSession.shared.dataTask(with: urlRequest) { (data, response, error) in
            DispatchQueue.main.async {
                print("finished")
                var image : YYImage?
                if let data = data, error == nil {
                    image = YYImage(data:data)
                }
                
                self.finish(fetchType, image:image, error:error)
            }
        }
        startDataTask(fetchType, downloadTask)
    }
    
    func finish(_ fetchType: FetchType, image: YYImage? = nil, error: Error? = nil) {
        if fetchType == .thumbnail {
            if let image = image {
                thumbnailImage = image
            }
            if let error = error {
                thumbnailError = error
            }
            thumbnailDelegate?.gifItem(self, loadedThumbnail: thumbnailImage, error: thumbnailError)
        } else {
            if let image = image {
                fullsizeImage = image
            }
            if let error = error {
                fullsizeError = error
            }
            fullsizeDelegate?.gifItem(self, loadedFullsize: fullsizeImage, error: fullsizeError)
        }
    }
    
    func startDataTask(_ fetchType: FetchType, _ dataTask: URLSessionDataTask? = nil) {
        if fetchType == .thumbnail {
            if let dataTask = dataTask {
                thumbnailDownloadTask = dataTask
            }
            thumbnailDownloadTask?.resume()
        } else {
            if let dataTask = dataTask {
                fullsizeDownloadTask = dataTask
            }
            fullsizeDownloadTask?.resume()
        }
    }
    
    func handleExistingDownloadTask(_ fetchType: FetchType) -> Bool {
        guard let downloadTask = (fetchType == .thumbnail) ? thumbnailDownloadTask : fullsizeDownloadTask else {
            return false
        }
        
        switch downloadTask.state {
        case .running:
            break
        case .completed:
            finish(fetchType)
        case .canceling:
            break
        case .suspended:
            startDataTask(fetchType)
            print("resuming")
        }
        return true
    }
    
    func pauseThumbnailDownload() {
        if let thumbnailDownloadTask = thumbnailDownloadTask,
            thumbnailDownloadTask.state != .completed {
            print("pausing")
            thumbnailDownloadTask.suspend()
        }
    }
    
    func cancelThumbnailDownload() {
        if let thumbnailDownloadTask = thumbnailDownloadTask,
            thumbnailDownloadTask.state != .completed {
            print("cancelling")
            thumbnailDownloadTask.cancel()
        }
    }
    
    func restartThumbnailDownload() {
        cancelThumbnailDownload()
        print("restarting")
        thumbnailDownloadTask = nil
        thumbnailError = nil
        thumbnailDelegate?.gifItemWillRestartThumbnailDownload(self)
        loadThumbnail()
    }
}
