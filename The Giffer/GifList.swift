//
//  GifList.swift
//  The Giffer
//
//  Created by Philip White on 3/28/19.
//  Copyright © 2019 Philip White. All rights reserved.
//

import UIKit
import GiphyCoreSDK

protocol GifListDelegate: class {
    func gifList(_ gifList: GifList, didFinishLoading error: Error?)
}

class GifList: NSObject {
    private var operation : Operation?
    private var itemCache : [String : GifItem] = [:]
    
    let searchString : String
    weak var delegate : GifListDelegate?
    var mediaResponse : GPHListMediaResponse?
    
    var hasFailed : Bool = false
    var hasFinished : Bool = false
    var isExecuting : Bool = false
    
    var count : Int {
        if let i = mediaResponse?.data?.count {
            return i
        }
        return 0
    }
    
    init(searchString: String) {
        self.searchString = searchString
        super.init()
    }
    
    func start() {
        isExecuting = true
        operation = GiphyCore.shared.search(searchString, limit: 100) { (response, error) in
            DispatchQueue.main.async {
                self.hasFinished = (error == nil)
                self.hasFailed = (error != nil)
                self.mediaResponse = response
                self.delegate?.gifList(self, didFinishLoading: error)
            }
        }
    }
    
    func cancel() {
        operation?.cancel()
        
        for (_,item) in itemCache {
            item.cancelThumbnailDownload()
        }
    }
    
    @discardableResult
    func loadItem(at index: Int) -> GifItem {
        let item = self.item(at: index)
        
        // It's okay to call this multiple times
        item.loadThumbnail()
        
        return item
    }
    
    func item(at index: Int) -> GifItem {
        let id = mediaResponse!.data![index].id
        var item = itemCache[id]
        if item == nil {
            item = GifItem(mediaResponse!.data![index])
            itemCache[id] = item
        }
        return item!
    }
    
    func pauseLoadingOfItem(at index: Int) {
        let id = mediaResponse!.data![index].id
        if let item = itemCache[id] {
            item.pauseThumbnailDownload()
        }
    }
}
