//
//  GifViewController.swift
//  The Giffer
//
//  Created by Philip White on 4/1/19.
//  Copyright © 2019 Philip White. All rights reserved.
//

import UIKit
import YYImage

// TODO: Unload/cancel downloading when view controller is dismissed

class GifViewController: UIViewController , GifItemFullsizeDelegate{

    var imageView : YYAnimatedImageView!
    var spinnerView : UIActivityIndicatorView!
    let gifItem : GifItem
    
    init(gifItem: GifItem) {
        self.gifItem = gifItem
        super.init(nibName: nil, bundle: nil)
        self.title = gifItem.title
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imageView = YYAnimatedImageView(image: gifItem.thumbnailImage)
        
        view = UIView()
        
        view.addSubview(imageView)
        imageView.backgroundColor = UIColor.white
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        imageView.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        imageView.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        imageView.heightAnchor.constraint(equalTo: view.heightAnchor).isActive = true
        imageView.contentMode = .scaleAspectFit
        imageView.autoPlayAnimatedImage = false
        
        spinnerView = UIActivityIndicatorView()
        
        let barButton = UIBarButtonItem(customView: spinnerView)
        navigationItem.rightBarButtonItem = barButton;
        spinnerView.color = UIColor.black
        spinnerView.startAnimating()
        
        gifItem.fullsizeDelegate = self
        gifItem.loadFullsizeImage()
    }
    
    func gifItem(_ gifItem: GifItem, loadedFullsize image: YYImage?, error: Error?) {
        spinnerView.stopAnimating()
        if let image = image,
            error == nil {
            imageView.autoPlayAnimatedImage = true
            imageView.image = image
        } else {
            imageView.contentMode = .center
            imageView.image = UIImage(named: "ErrorLarge")
        }
    }
    
    
}
