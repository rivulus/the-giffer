//
//  GifCollectionViewController.swift
//  The Giffer
//
//  Created by Philip White on 3/28/19.
//  Copyright © 2019 Philip White. All rights reserved.
//

import UIKit
import CHTCollectionViewWaterfallLayout

fileprivate enum Constants {
    static let imageCellReuseIdentifier = "ImageCell"
    static let loadingCellReuseIdentifier = "LoadingCell"
    static let labelCellReuseIdentifier = "LabelCell"
    static let thumbnailWidth : CGFloat = 200.0
    static let soleItemDimension : CGFloat = 250.0 // squared
    static let minimumMarginWidth : CGFloat = 2.0
    static let minimumInteritemSpacing : CGFloat = 4.0
}

fileprivate enum SearchState {
    case noSearchQuery
    case isSearching
    case hasResults
    case noResults
    case hasFailed
}
    
class GifCollectionViewController: UICollectionViewController, UISearchResultsUpdating, CHTCollectionViewDelegateWaterfallLayout, UICollectionViewDataSourcePrefetching, GifListDelegate{
    
    private var searchController: UISearchController!
    private var gifList : GifList?
	
    private var searchState : SearchState {
        if let gifList = gifList {
            if gifList.hasFinished {
                if gifList.count == 0 {
                    return .noResults
                } else {
                    return .hasResults
                }
            } else if gifList.isExecuting {
                return .isSearching
            } else if gifList.hasFailed {
                return .hasFailed
            }
        }
        return .noSearchQuery
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        searchController = UISearchController(searchResultsController: nil)
        searchController.searchResultsUpdater = self
        searchController.searchBar.autocapitalizationType = .none
        searchController.obscuresBackgroundDuringPresentation = false
        navigationItem.searchController = searchController
        navigationItem.hidesSearchBarWhenScrolling = false
        
        definesPresentationContext = true

        // Register cell classes
        collectionView.register(GifCollectionViewCell.self, forCellWithReuseIdentifier: Constants.imageCellReuseIdentifier)
        collectionView.register(GifCollectionViewLoadingCell.self, forCellWithReuseIdentifier: Constants.loadingCellReuseIdentifier)
        // Do any additional setup after loading the view.
    }

    // MARK: UICollectionViewDataSource

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }

    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch searchState {
        case .hasResults:
            return gifList!.count
        default:
            return 1 // Label, or spinner, or error symbol
        }
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        switch searchState {
        case .noResults:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Constants.labelCellReuseIdentifier, for: indexPath) as! GifCollectionLabelViewCell
            cell.configureAsNoResults()
            return cell
        case .hasResults:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Constants.imageCellReuseIdentifier, for: indexPath) as! GifCollectionViewCell
            cell.gifItem = gifList!.loadItem(at: indexPath.item)
            cell.configureViews()
            return cell
        case .isSearching:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Constants.loadingCellReuseIdentifier, for: indexPath) as! GifCollectionViewLoadingCell
            cell.configureAsSpinner()
            return cell
        case .hasFailed:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Constants.loadingCellReuseIdentifier, for: indexPath) as! GifCollectionViewLoadingCell
            cell.configureAsError()
            return cell
        case .noSearchQuery:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Constants.labelCellReuseIdentifier, for: indexPath) as! GifCollectionLabelViewCell
            cell.configureAsNoSearch()
            return cell
        }
    }

    // MARK: UICollectionViewDelegate
    override func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        if searchState == .hasResults {
            gifList!.pauseLoadingOfItem(at: indexPath.item)
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if searchState == .hasResults {
            let gifItem = gifList!.item(at: indexPath.item)
            
            if gifItem.thumbnailError != nil {
                gifItem.restartThumbnailDownload()
            } else {
                let gifViewController = GifViewController(gifItem: gifItem)
                self.navigationController?.pushViewController(gifViewController, animated: true)
            }
        }
    }

    // MARK: CHTCollectionViewDelegateWaterfallLayout
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if searchState == .hasResults {
            let gifItem = gifList!.item(at: indexPath.item)
            if let thumbnailMetadata = gifItem.thumbnailMetadata {
                return CGSize(width: thumbnailMetadata.width, height: thumbnailMetadata.height)
            } else {
                // We shouldn't hit this unless the server response is missing data
                return CGSize(width:Constants.thumbnailWidth, height:Constants.thumbnailWidth/*intentional*/)
            }
        } else {
            return CGSize(width: Constants.soleItemDimension, height: Constants.soleItemDimension)
        }
    }
 
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        if searchState == .hasResults {
            let columnCount = CGFloat(searchResultsColumnCount)
            
            let extraSpace = collectionView.bounds.width.truncatingRemainder(dividingBy: columnCount * Constants.thumbnailWidth)
            let margin = (extraSpace - columnCount * Constants.minimumInteritemSpacing) / 2.0
            
            return UIEdgeInsets(top: 0.0, left: margin, bottom: 0.0, right: margin)
        } else {
            let margin = (collectionView.bounds.width - Constants.soleItemDimension) / 2.0
            
            return UIEdgeInsets(top: 0.0, left: margin, bottom: 0.0, right: margin)
        }
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        if searchState == .hasResults {
            return Constants.minimumInteritemSpacing
        } else {
            return 0.0
        }
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, columnCountForSection section: Int) -> Int {
        if searchState == .hasResults {
            return searchResultsColumnCount
        } else {
            return 1
        }
    }
    
    private var searchResultsColumnCount : Int {
        if searchState != .hasResults {
            fatalError()
        }
        
        let columnCount = (collectionView.bounds.width / Constants.thumbnailWidth).rounded(.towardZero)
        let extraSpace = collectionView.bounds.width.truncatingRemainder(dividingBy: columnCount * Constants.thumbnailWidth)
        let minimumExtraSpace = Constants.minimumMarginWidth * 2.0 + Constants.minimumInteritemSpacing * (columnCount - 1.0) // margins and between columns
        
        if minimumExtraSpace > extraSpace {
            return Int(columnCount) - 1
        } else {
            return Int(columnCount)
        }
        
    }
    
    // MARK: UICollectionViewDataSourcePrefetching
    func collectionView(_ collectionView: UICollectionView, prefetchItemsAt indexPaths: [IndexPath]) {
        if let gifList = gifList,
            gifList.hasFinished {
            for indexPath in indexPaths {
                gifList.loadItem(at: indexPath.item)
            }
        }
    }
    
    // MARK: UISearchResultsUpdating
    func updateSearchResults(for searchController: UISearchController) {
        if let searchString = navigationItem.searchController?.searchBar.text,
            searchString != gifList?.searchString {
            gifList?.cancel()
            gifList?.delegate = nil
            gifList = nil
            
            if searchString.count > 0 {
                gifList = GifList(searchString: searchString)
                gifList?.delegate = self
                gifList?.start()
            }
            collectionView.reloadData()
        }
    }
    
    // MARK: GifListDelegate
    func gifList(_ gifList: GifList, didFinishLoading error: Error?) {
        collectionView.reloadData()
    }
}
